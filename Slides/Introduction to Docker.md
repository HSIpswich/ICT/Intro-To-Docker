class: middle, center
<!-- Insert Logo here -->

![](image\HS_Logo_WIDE_GoogleLogo.png)

# Introduction to Docker

### Presenter: Robert Manietta<!-- Change name as appropriate -->

### Cause Leader IT&T <!-- Change title as appropriate -->

???
Welcome, Housekeeping, attendance - > on lists

---
class: middle
## What is Docker?

- Docker is an tool set written for interacting with Linux Containers

## What is a container?

- A container is a runtime instance of an image. It is what becomes in memor when actually executed. Running in complete isolation from the host environment by default, only accessing host files and network ports if configured to do so.

## What is an image?

- An image is a lightwight, stand-alone, executable package that contains all of the code, runtimes, libraries, environment variables and config files to run a specific application / service.

---
class: middle
## Installing docker?

Docker is now available for Linux, MAC, and Windows.

https://www.docker.com/community-edition#/Download

> NOTE: version 1.13 or higher will be needed as we go forward.

---
class: middle
## Testing the installation.

~~~
docker run hello-world
~~~

## Checking our Version number.

~~~
docker --version
~~~

---
class: middle

## Building our first docker app

Building an app the docker way we start at the bottom of a hierarchy the container, moving then to the service, and finally to the stack.

The old way, install runtime environment for python, have it all set up like so and write your app, then when it comes time to deploy you have to setup the server the exact same way or oh no the app doesn't work now.

With docker though we create a protable runtime environment that get's assembled with the app code and can be transported in one package between development and the live servers.

These portable images are defined by something called a 'dockerfile'.

---
class: middle

## Lets Try it out?

1. Create an empty directory
2. move into the directory you just made and create a file called 'Dockerfile', adding the following to the file.

---
class: middle

~~~
#Use and official python runtime as a parent image
FROM python:2.7-slim

# Set the working directory to /app
WORKINGDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

#Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variables.
ENV NAME world

# Run app.py when the container launches.
CMD ["python", "app.py"]
~~~

>You may notice this dockerfile referes to some files we don't have like app.py and requirements.txt so lets fix that now.

---
class: middle

## requirements.txt

```
Flask
Redis
```
---
class: middle

#### app.py
```python
from flask import flask
from redis import Redis, RedisError
import os
import socket

# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask (__name__)

@app.route("/")
def hello():
  try:
    visits = redis.incr("counter")
  except RedisError:
    visits = "<i>cannont connect to Redis, counter disabled</i>"

  html = "<h3>Hello {name}!</h3>" \
         "<b>Hostname:</b> {hostname}<br/>" \
         "<b>Visits:</b> {visits}"
  return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits)

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=80)
```
---
class: middle

## Lets Build this container.

~~~
docker build -t friendlyhello .
~~~

## Where is my built image?

Built images are added to your local machine's local docker image repository.

---
class: middle

## Let's Run our app now?

~~~
docker run -p 4000:80 friendlyhello
~~~

## how can we check our app works?

Open a web browser and visit.

http://localhost:4000

We close the app with "CTRL + C"

We could run our app in a detached mode by using

~~~
docker run -d -p 4000:80 friendlyhello
~~~

---
class: middle

## Sharing your image

Docker has a built in public registry we can publish to by default to do this though you will need to make an account at.

http://cloud.docker.com

Take note of your username and password you still need it.

~~~
docker login
~~~

## Tag it

now let's tag it with a nice name to make finding it easier. docker tag image <username>/repository:tag

~~~
docker tag image hsipswich/get-started:part1
~~~

run docker images to see the newly tagged image

~~~
docker image ls
~~~

---
class: middle

## Time to upload for all

~~~
docker push <username>/repository:tag
~~~

## Now you can run this from any machine with docker by calling

~~~
docker run -p 4000:80 <username>/repository:tag
~~~

---
class: middle

## Services

In a distributed application, different pieces of the appliaction are called services.

For example a website my have a few separate services, the web front-end what a visitor will see, the database services, and an email service, a backend web service.

Luckily with docker these are easy to define, run and scale by using a docker-compose.yml file.

---
### Our first docker-compose.yml file

Ensure you have pushed the image we created before.
Now create a new file called docker-compose.yml containing the below:
~~~
version: "3"
services:
  web:
    # replace username/repository:tag with your name and image details.
    image: username/repository:tag
    deploy:
      replicas: 5
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
      restart_policy:
        condition: on-failure
    ports:
      - "80:80"
    networks:
      - webnet
  networks:
    webnet:
~~~

---
class: middle

## Let's run our first load-balanced application

Run

~~~
docker swarm init
~~~

Name your application

~~~
docker stack deploy -c docker-compose.yml getstartedlab
~~~

Runing this will show that we do have more than one instance running.

~~~
docker stack ps getstartedlab
~~~

> To scale the app you just change the number of replicas in the docker-compose.yml file and rerun the docker stack deploy. No need to tear the stack down docker will do an inplace update.

---
class: middle

## Shutting down our app.

~~~
docker stack rm getstartedlab
~~~

Now take down the swarm container with

~~~
docker swarm leave --force
~~~

---
class: middle
## What now?

#### HSIpswich welcomes you to practice your new skills at our Public Open times, as these vary please check our website.
http://www.hsipswich.org.au
##### Once your participation certificate is presented you are free to leave.
<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
