## HSIpswich  ~ Docker Cheat Sheet

 docker --version  

 docker run hello-world

 docker stop [name/id]

 docker ps -a

 docker build -t friendlyhello

 docker run -d -p 80:80 friendlyhello

 docker run -p 80:80 friendlyhello

 docker login

 docker tag image <username>/repository

 docker images ls

 docker push <username>/repository

 docker run -p 4000:4000 <username>/repository:tag

 docker swarm init

 docker swarm join

 docker stack deploy -c docker-compose.yml [image name]

 docker stack ps [image name]

 docker stack rm [image name]

 docker swarm leave --force

docker pull rayeshuang/friendlyhello

---
## Dockerfile
~~~
#Use and official python runtime as a parent image
FROM python:2.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

#Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variables.
ENV NAME world

# Run app.py when the container launches.
CMD ["python", "app.py"]
~~~

---

## requirements.txt

```
Flask
Redis
```

---

## app.py

```python
from flask import flask
from redis import Redis, RedisError
import os
import socket

# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask (__name__)

@app.route("/")
def hello():
  try:
    visits = redis.incr("counter")
  except RedisError:
    visits = "<i>cannont connect to Redis, counter disabled</i>"

  html = "<h3>Hello {name}!</h3>" \
         "<b>Hostname:</b> {hostname}<br/>" \
         "<b>Visits:</b> {visits}"
  return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits)

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=80)
```

<hr />

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
