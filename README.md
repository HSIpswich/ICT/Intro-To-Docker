# Introduction To Docker (Linux Containers)

### Materials Compiled and made by HS Ipswich (members) for Workshop / Course.


This course material covers an Introduction to Docker (Linux Containers) for beginners to develop scalling services.

This course teachers how to use Docker to build infinately scalling web services.

Many companies are now taking advantage of the many benefits Docker brings over other technologies to leverage their infrustructure data and data services.

Wanting to learn more these resources may help.

### Note's:

Slides within this repository are written in markdown specific for making remark slides useing the remarkjs styling.

### Licence Information

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
