## Docker Assignment, Swarm guided projects

You have been completing throughout the course a series of miniture guided projects throughout the day.

These have demonstrated your abilities with the technologies and should have been regularly reviewed by the instructor during the course of the day.

No further assessment is to be conducted, unless chosen by the instructor to prove understanding of the technology. 

<hr>

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
